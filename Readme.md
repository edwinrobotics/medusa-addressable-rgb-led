# Medusa Addressable RGB LED

![Medusa Addressable RGB LED](https://shop.edwinrobotics.com/3969-thickbox_default/medusa-addressable-rgb-led.jpg "Medusa Addressable RGB LED")

[Medusa Addressable RGB LED](https://shop.edwinrobotics.com/new-products/1182-medusa-addressable-rgb-led.html)


The Medusa Addressable RGB LED utilizes the ever so popular WS2812 allowing to control multiple LEDs using just one pin. The WS2811 controller present in this is capable of providing 8 bits per channel, this translates to over 16 Million colors and creates no limits to your DIY enthusiasm.


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.